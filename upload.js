/**
 * Created by lukas on 10.11.15.
 */
'use strict';



module.exports = function (config) {
    var express = require('express');
    var multiparty  = require('multiparty');
    var FS = require('q-io/fs');
    var Q = require('q');

    var app = express();

    var ifNotDirCreate = function (directory) {
        return FS.isDirectory(directory)
            .then(function (yes) {
                if (!yes) {
                    return FS.makeDirectory(directory);
                } else {
                    return yes;
                }
            });
    };

    var createDbImage = function (directory, filename, userId) {
        console.log(directory, filename, userId);
        return config.getRepository('Image')
            .then(function (repository) {
                var nameArray = filename.split('.');
                var ext = nameArray.pop();
                return repository.createEntity({
                    directory: directory,
                    filename: nameArray.join('.'),
                    extension: ext
                }, userId);
            })
            .then(function (entity) {
                return config.getAcl().isEntityAllowed(userId, entity, 'write');
            })
            .then(function (entity) {
                if (!entity) {
                    config.getError().forbidden();
                }
                return entity.save();
            });
    };

    var uploadImage = function (file, directory, userId) {
        var originalDirectory = directory;

        directory = directory.replace('../', '');
        directory = '../public/data/images/' + directory;

        return ifNotDirCreate(directory)
            .then(function () {
                return createDbImage(originalDirectory, file.originalFilename, userId);
            })
            .then(function (entity) {
                return FS.move(file.path, directory + entity.get('id'))
                    .then(function () {
                        return entity;
                    });
            });
    };

    app.post('/images', function (req, res) {
        var form = new multiparty.Form();
        var parsedPromise = Q.defer();

        form.parse(req, function(err, fields, files) {
            var filePromises = [];
            var directory = fields.directory && fields.directory[0] ? fields.directory[0] + '/' : '';

            for (var i in files) {
                if (files.hasOwnProperty(i)) {
                    files[i].forEach(function (file) {
                        filePromises.push(uploadImage(file, directory, req.userId));
                    });
                }
            }

            parsedPromise.resolve(Q.all(filePromises));
        });


        parsedPromise.promise
            .then(function () {
                res.send({ok: 1});
            })
            .catch(function (err) {
                console.log(err);
            });
    });

    app.post('/image', function (req, res) {
        var form = new multiparty.Form();
        var parsedPromise = Q.defer();

        form.parse(req, function(err, fields, files) {
            var file = files.file || [];
            console.log(file);

            var directory = fields.directory && fields.directory[0] ? fields.directory[0] + '/' : '';

            if (file.length === 1) {
                parsedPromise.resolve(uploadImage(file[0], directory, req.userId));
            } else {
                parsedPromise.reject('Ne');
            }
        });


        parsedPromise.promise
            .then(function (entity) {
                res.send(entity);
            })
            .catch(function (err) {
                console.log(err);
            });
    });

    return app;
};
