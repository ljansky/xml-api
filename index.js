/**
 * Created by lukas on 17.8.15.
 */
'use strict';

module.exports = function (config) {
    var express = require('express');
    var router = express.Router();

    var bodyParser = require("body-parser");
    var validation = require('./validation')(config);
    var auth = require('./auth')(config);
    var menu = require('./menu')(config);
    var resource = require('./resource')(config);
    var upload = require('./upload')(config);
    var image = require('./image')(config);

    router.use(bodyParser.urlencoded({ extended: false }));
    router.use(bodyParser.json());

    router.use('/auth', auth);
    router.use(auth.authMiddleware);
    router.use('/menu', menu);
    router.use('/resource', resource);
    router.use('/validate', validation);
    router.use('/upload', upload);
    router.use('/image', image);

    return router;
};