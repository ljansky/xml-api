/**
 * Created by lukas on 17.8.15.
 */
'use strict';

function sendError (err, res) {
    var code = err.code || 500;
    var messages = err.messages || err;
    res.status(code)
        .send(messages);
}

module.exports = function (config) {
    var express = require('express');
    var router = express.Router();

    router.get('/:repository', function (req, res) {
        var repositoryName = req.params.repository;
        config.getAcl().isRepositoryAllowed(req.userId, repositoryName, 'read').then(function () {
            var formatter = config.getFormatter(config.getFormatter().decodeFormat(req.query.format));

            var params = {
                order: req.query.order || [],
                where: [],
                join: req.query.join || null,
                limit: req.query.limit || 0,
                offset: req.query.offset || 0
            };

            var where = req.query.where || {};

            for (var i in where) {
                if (where.hasOwnProperty(i)) {
                    if (typeof where[i] === 'object') {
                        params.where.push([i, where[i][0], where[i][1]]);
                    } else {
                        params.where.push([i, '=', where[i]]);
                    }
                }
            }

            return config.getRepository(repositoryName).then(function (repository) {
                return repository.findAll(params, formatter.getRelations());
            })
            .then(function (data) {
                return config.getAcl().filterList(req.userId, data, 'read')
            })
            .then(function (data) {
                res.send(formatter.formatAll(data));
            });
        }).catch(function (err) {
            sendError(err, res);
        }).done();
    });

    router.get('/:repository/like', function (req, res) {
        var repositoryName = req.params.repository;
        config.getAcl().isRepositoryAllowed(req.userId, repositoryName, 'read').then(function () {
            var fields = req.query.fields;
            var value = req.query.value;

            var format = {
                id: 1
            };

            var order = [];

            for (var i = 0; i < fields.length; i++) {
                format[fields[i]] = 1;
                order.push([fields[i], 'asc']);
            }

            var formatter = config.getFormatter(format);

            return config.getRepository(req.params.repository).then(function (repository) {
                return repository.findAll({
                    where: [['CONCAT(' + fields.join(', " ", ') + ') LIKE ?', ['%' + value + '%']]],
                    order: order
                });
            }).then(function (data) {
                res.send(formatter.formatAll(data));
            });
        }).catch(function (err) {
            sendError(err, res);
        }).done();
    });

    router.get('/:repository/related', function (req, res) {

        var repositoryName = req.params.repository;
        var formatter = config.getFormatter(config.getFormatter().decodeFormat(req.query.format));

        config.getAcl().isRepositoryAllowed(req.userId, repositoryName, 'read').then(function () {
            var field = req.query.field;
            var value = req.query.value;

            return config.getRepository(req.params.repository).then(function (repository) {
                return repository.findAll({
                    where: [[field, '=', value]]
                });
            }).then(function (data) {
                res.send(formatter.formatAll(data));
            });
        }).catch(function (err) {
            sendError(err, res);
        }).done();
    });

    router.post('/:repository/tree', function (req, res) {

        var repositoryName = req.params.repository;

        config.getAcl().isRepositoryAllowed(req.userId, repositoryName, 'read').then(function () {
            var titleResolver = config.getTitleResolver(req.body.title);
            var grouper = config.getGrouper(req.body.groups, titleResolver);

            var formatter = config.getFormatter(grouper.getFormat());

            var rootTitle = req.body.root;
            var groups = req.body.groups;

            return config.getRepository(req.params.repository).then(function (repository) {
                return repository.findAll({}, grouper.getRelations());
            })
            .then(function (data) {
                return config.getAcl().filterList(req.userId, data, 'write')
            })
            .then(function (data) {

                var tree = {
                    title: rootTitle,
                    data: {},
                    folderId: 0
                };

                var d = formatter.formatAll(data);
                tree.data = grouper.groupBy(d, groups);

                res.send(tree);
            });
        }).catch(function (err) {
            sendError(err, res);
        }).done();
    });

    router.get('/:repository/:id', function (req, res) {
        var repositoryName = req.params.repository;
        config.getAcl().isRepositoryAllowed(req.userId, repositoryName, 'read').then(function () {
            var formatter = config.getFormatter(config.getFormatter().decodeFormat(req.query.format));

            return config.getRepository(req.params.repository).then(function (repository) {
                return repository.findOne({id: req.params.id}, formatter.getRelations());
            }).then(function (entity) {
                res.send(formatter.format(entity));
            });
        }).catch(function (err) {
            sendError(err, res);
        }).done();
    });

    router.put('/:repository/:id', function (req, res) {

        var loadedRepository = null;

        config.getRepository(req.params.repository)
            .then(function (repository) {
                loadedRepository = repository;
                return repository.findOne({id: req.params.id}, [loadedRepository.config.ownerParent]);
            })
            .then(function (entity) {
                entity.setTempData(req.body);
                return config.getAcl().isEntityAllowed(req.userId, entity, 'write');
            })
            .then(function (entity) {
                if (!entity) {
                    config.getError().forbidden();
                }
                loadedRepository.getValidator().validate(req.body);
                return entity.save(req.body);
            })
            .then(function () {
                res.send(req.body);
            })
            .catch(function (err) {
                sendError(err, res);
            }).done();
    });

    router.post('/:repository', function (req, res) {

        config.getRepository(req.params.repository)
            .then(function (repository) {
                repository.getValidator().validate(req.body);
                return repository.createEntity(req.body, req.userId, [repository.config.ownerParent]);
            })
            .then(function (entity) {
                return config.getAcl().isEntityAllowed(req.userId, entity, 'write');
            })
            .then(function (entity) {
                if (!entity) {
                    config.getError().forbidden();
                }
                return entity.save();
            })
            .then(function (entity) {
                res.send(entity);
            })
            .catch(function (err) {
                sendError(err, res);
            }).done();
    });

    router.delete('/:repository/:id', function (req, res) {

        var loadedRepository = null;
        var entityId = parseInt(req.params.id);

        config.getRepository(req.params.repository)
            .then(function (repository) {
                if (!entityId) {
                    config.getError().notFound();
                }

                loadedRepository = repository;
                return repository.findOne({id: entityId}, [loadedRepository.config.ownerParent]);
            })
            .then(function (entity) {
                return config.getAcl().isEntityAllowed(req.userId, entity, 'write');
            })
            .then(function (entity) {
                if (!entity) {
                    config.getError().forbidden();
                }
                return entity.delete();
            })
            .then(function () {
                res.send({id: entityId});
            })
            .catch(function (err) {
                sendError(err, res);
            }).done();
    });

    return router;
}
