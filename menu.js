/**
 * Created by lukas on 19.9.15.
 */
'use strict';

module.exports = function (config) {

    var express = require('express');
    var router = express.Router();

    router.get('/', function (req, res) {

        config.getAcl().getUserRoles(req.userId)
            .then(function (roles) {
                return config.getRepository('RoleMenu')
                    .then(function (repository) {
                        return repository.findAll({
                            where: [['role_id', 'in', roles]]
                        }, ['Menu']);
                    });
            })
            .then(function (data) {

                var items = [];
                var itemsMap = {};
                //todo: udelat vice urovni
                data.forEach(function (item) {

                    var it = {
                        id: item.get('menu_id'),
                        parent: item.related('Menu').get('parent_id'),
                        link: item.related('Menu').get('module') + '/0',
                        title: item.related('Menu').get('title'),
                        subItems: []
                    };

                    if (typeof itemsMap[it.id] === 'undefined') {
                        itemsMap[it.id] = it;
                    }
                });

                for (var i in itemsMap) {
                    if (itemsMap.hasOwnProperty(i)) {
                        if (itemsMap[i].parent && typeof itemsMap[itemsMap[i].parent] !== 'undefined') {
                            itemsMap[itemsMap[i].parent].subItems.push(itemsMap[i]);
                        } else {
                            items.push(itemsMap[i]);
                        }
                    }
                }

                res.send(items);
            });
    });

    return router;
}
