/**
 * Created by lukas on 17.9.15.
 */
'use strict';

module.exports = function (config) {
    var express = require('express');
    var router = express.Router();
    var jwt = require('jsonwebtoken');
    var crypto = require('crypto');

    var secret = 'totoJeSecret';

    router.post('/authenticate-app', function (req, res) {

    });

    router.post('/authenticate', function (req, res) {

        var params = {
            email: req.body.email
        };

        config.getRepository('User').then(function (repository) {
            return repository.findOne(params);
        }).then(function (data) {
            if (!data) {
                config.getError().unauthorized();
            } else {
                var hash = crypto.createHash('sha1').update(req.body.password).digest('hex');
                //todo: store access token

                if (data.get('password') === hash) {

                    var user = {
                        id: data.get('id'),
                        logged: Date.now()
                    };

                    var token = jwt.sign(user, secret, {
                        expiresInMinutes: 1440 // expires in 24 hours
                    });

                    res.send({ok: true, token: token});
                } else {
                    config.getError().unauthorized();
                }
            }
        }).catch(function (err) {
            res.status(err.code)
                .send(err.messages);
        });
    });

    router.post('/logout', function (req, res) {
        var token = req.body.token || req.query.token || req.headers['x-access-token'];

        if (token) {
            jwt.verify(token, secret, function(err, decoded) {
                if (err) {
                    return res.send({ ok: false });
                } else {
                    config.getAcl().removeUser(decoded.id);
                    return res.send({ ok: true });
                }
            });
        } else {
            return res.send({ ok: false });
        }
    });

    router.get('/logged', function (req, res) {
        var token = req.body.token || req.query.token || req.headers['x-access-token'];

        if (token) {
            jwt.verify(token, secret, function(err, decoded) {
                if (err) {
                    return res.send({ ok: false });
                } else {
                    return res.send({ ok: true });
                }
            });
        } else {
            return res.send({ ok: false });
        }
    });

    router.authMiddleware = function (req, res, next) {
        req.userId = 0;
        // check header or url parameters or post parameters for token
        var token = req.body.token || req.query.token || req.headers['x-access-token'];

        // decode token
        if (token) {
            // verifies secret and checks exp
            jwt.verify(token, secret, function(err, decoded) {

                if (!err) {
                    req.userId = decoded.id;
                }

                next();
            });

        } else {
            next();
        }
    };

    return router;
}