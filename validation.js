/**
 * Created by lukas on 14.9.15.
 */
'use strict';

module.exports = function (config) {
    var express = require('express');
    var router = express.Router();

    router.get('/duplicate', function (req, res) {
        var fields = req.query.fields;

        if (typeof req.query.id !== 'undefined' && req.query.id > 0) {
            res.send({ok: true});
        } else {
            config.getRepository(req.query.repository).then(function (repository) {
                var conditions = [];
                for (var i in fields) {
                    if (fields.hasOwnProperty(i)) {
                        conditions.push([i, 'LIKE', fields[i]]);
                    }
                }
                return repository.findAll({
                    where: conditions
                });

            }).then(function (data) {
                res.send({ok: data.length === 0});
            });
        }
    });

    return router;
};