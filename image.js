/**
 * Created by lukas on 15.11.15.
 */
'use strict';

module.exports = function (config) {
    var express = require('express');
    var FS = require('q-io/fs');
    var Q = require('q');
    var fs = require('fs');
    var easyimg = require('easyimage');

    var app = express();
    var imagesDirectory = __dirname + '/../../public/data/images/';

    var createImageWithStyle = function (imagePath, imageId, styleId, imageName) {
        var imageInfo = null;

        return FS.isFile(imagePath + imageId)
            .then(function (exists) {
                if (!exists) {
                    config.getError().notFound('Image not found');
                }

                return easyimg.info(imagePath + imageId)
            })
            .then(function (info) {
                imageInfo = info;
                return config.getRepository('ImageStyle');
            })
            .then(function (repository) {
                return repository.findOne({
                    id: styleId
                })
            })
            .then(function (style) {
                if (!style.model) {
                    config.getError().notFound('Style not found');
                }

                var imageWidthHeightRatio = imageInfo.width / imageInfo.height;
                var styleWidthHeightRation = style.get('width') / style.get('height');

                var width = 0;
                var height = 0;

                if (imageWidthHeightRatio < styleWidthHeightRation) {
                    width = style.get('width');
                    height = imageInfo.height * styleWidthHeightRation;
                } else {
                    width = imageInfo.width / styleWidthHeightRation;
                    height = style.get('height');
                }

                return easyimg.rescrop({
                    src: imagePath + imageId,
                    dst: imagePath + imageName,
                    width: width,
                    height: height,
                    cropwidth: style.get('width'),
                    cropheight: style.get('height')
                });
            });
    }

    app.get('/:imageId/:styleId', function (req, res) {

        var imageId = req.params.imageId;
        var styleId = req.params.styleId;
        var imageName = imageId + '_' + styleId + '.';
        var imagePath = '';
        var dbImage = null;

        config.getRepository('Image')
            .then(function (repository) {
                return repository.findOne({
                    id: imageId
                })
            })
            .then(function (image) {
                imageName += image.get('extension');
                imagePath = imagesDirectory + image.get('directory') + '/';
                dbImage = image;
                return FS.isFile(imagePath + imageName);
            })
            .then(function (exists) {
                if (exists) {
                    var readStream = fs.createReadStream(imagePath + imageName);
                    readStream.pipe(res);
                } else {
                    return createImageWithStyle(imagePath, imageId, styleId, imageName)
                        .then(function () {
                            var readStream = fs.createReadStream(imagePath + imageName);
                            readStream.pipe(res);
                        });
                }
            })
            .catch(function (err) {
                console.log(err);
                res.status(err.code)
                    .send(err.messages);
            });
    });

    return app;
};